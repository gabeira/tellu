## TellU App

This is an Android Feedback application, to send messages to Teammates, thanking for being helpful to the Team toward our Goals.

## Motivation

Psychology has proven long time ago, that Positive Feedback increase feelings of competence and give the subjects more intrinsic motivation (i.e. your own desire to do something), the only kind that reliably and sustainably makes us work towards a goal.

## Installation

This Project still under development, you can [Download our Sample App here](https://rink.hockeyapp.net/apps/c25e918e1c0f4ebc8f39580f535a88a0) or configure for your own Team following the steps below:

- Download the code from this Bitbucket Repository via [Android Studio](https://youtu.be/Z98hXV9GmzY) or command line running:
    
      git clone https://bitbucket.org/gabeira/tellu.git   

- Configure Google Firebase project for your team:
  - Create new Project on [Firebase Console](https://console.firebase.google.com);
  - Add Android mobile app to the project, for the `Android package name` use `mobi.tellu`, the same value specified in the `app/build.gradle` file as `applicationId`;
  - Set `Debug signing certificate SHA-1` using your fingerprint key, [check here how to get it](https://developers.google.com/android/guides/client-auth);
  - Setup the project to use Authentication, go to Sign-in Method, and Enable Google as a Sign-in Provider, if not set, you need to add the SHA-1 key configured before;
  - Download the generated `google-services.json` file, and replace it to the `app/` directory [here](https://bitbucket.org/gabeira/tellu/src/a7dca391492d34b2430c846d6fc013021439b02d/app/google-services.json) or get [more info here](https://support.google.com/firebase/answer/7015592#android).
  
- (Optional) Change the app download URL for Sharing purpose on `strings.xml` file with string name `share_app_url`;
- Finally, to Build the Project, you can use Android Studio or from command line just run:

      ./gradlew build

- (Optional) To install debug app from command line use:

      ./adb install /app/build/outputs/apk/app-debug.apk

## External Libs Reference

- [Google Firebase](https://github.com/firebase/quickstart-android)
- [Square OkHttp3](https://github.com/square/okhttp)
- [Bumptech Glide](https://github.com/bumptech/glide)
- [Apache Commons Lang](https://commons.apache.org/proper/commons-lang/)

## Tests

There is some small tests done, but essential for the functionalities, you can run on Android Studio or from the command line,
to run the Unit Tests just use:

    ./gradlew test
 
Also there is some Connected Android Tests, but this requires to have a device or emulator connected:

    ./gradlew connectedAndroidTest
