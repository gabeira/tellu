package mobi.tellu;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.List;

import mobi.tellu.util.MessageParser;

import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ParseMessageInstrumentedTest {
    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("mobi.tellu", appContext.getPackageName());
    }

    @Test
    public void messageEmoticonsParsing() throws Exception {

        Context appContext = InstrumentationRegistry.getTargetContext();

        List<String> emoticons = MessageParser.getEmoticons(
                appContext, "this is an (android) (arya) test with (nonimage) or more (chef).");

        assertEquals(3, emoticons.size());

        assertEquals(emoticons.get(0), "android");
        assertEquals(emoticons.get(1), "arya");
        assertEquals(emoticons.get(2), "chef");
    }
}
