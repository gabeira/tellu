package mobi.tellu;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ShareCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.apache.commons.lang3.StringUtils;

import mobi.tellu.fragment.BoardFragment;
import mobi.tellu.fragment.MessageFragment;
import mobi.tellu.fragment.MyFragment;
import mobi.tellu.fragment.TeammateFragment;
import mobi.tellu.model.MessageThanks;
import mobi.tellu.model.Teammates;
import mobi.tellu.util.Utilities;

public class MainActivity extends AppCompatActivity
        implements GoogleApiClient.OnConnectionFailedListener,
        NavigationView.OnNavigationItemSelectedListener,
        TeammateFragment.OnListFragmentInteractionListener {

    private static final String TAG = "MainActivity";
    public static final String ANONYMOUS = "Anonymous";
    public static final String DATABASE = "tellu";
    public static final String IS_FROM_NOTIFICATION = "redirect from notification";

    private String mUsername;
    private String mPhotoUrl;
    private String mUserEmail;
    private String myUserId;
    private SharedPreferences mSharedPreferences;

    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        mUsername = ANONYMOUS;

        // Initialize Firebase Auth
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();

        if (mFirebaseUser == null) {
            // Not signed in, launch the Sign In activity
            startActivity(new Intent(this, SignInActivity.class));
            finish();
            return;
        } else {
            myUserId = mFirebaseUser.getUid();
            if (mFirebaseUser.isAnonymous()) {
                mPhotoUrl = getString(R.string.anonymous_photo_url);
                mUserEmail = "empty email";
            } else {
                mUsername = mFirebaseUser.getDisplayName();
                mPhotoUrl = mFirebaseUser.getPhotoUrl().toString();
                mUserEmail = mFirebaseUser.getEmail();
            }
        }
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API)
                .build();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        if (mSharedPreferences.getBoolean(getString(R.string.pref_key_notification), false)) {
            setFirebaseMessagesListener();
        }

        if (getIntent().getBooleanExtra(IS_FROM_NOTIFICATION, false)) {
            String sender = getIntent().getStringExtra(MessageThanks.FROM_SENDER);
            if (StringUtils.isNotEmpty(sender)) {
                getTeammateByIdAndSelect(sender);
            } else {
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.container, MyFragment.newInstance(), getString(R.string.my_user))
                        .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                        .commit();
            }
        }

        if (savedInstanceState == null) {

            setupMyDeviceUser();

            if (mSharedPreferences.getBoolean(getString(R.string.pref_key_intro), true) &&
                    ((TellUApp) getApplication()).justOpen) {
                startActivity(new Intent(getApplicationContext(), IntroActivity.class));
            }

            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, TeammateFragment.newInstance(
                            getResources().getInteger(R.integer.grid_layout_size)))
                    .commit();

            ((TellUApp) getApplication()).justOpen = false;
        }

    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
            return true;
        }
        if (id == R.id.sign_out_menu) {
            mFirebaseAuth.signOut();
            Auth.GoogleSignInApi.signOut(mGoogleApiClient);
            mFirebaseUser = null;
            mUsername = ANONYMOUS;
            mPhotoUrl = null;
            mUserEmail = null;
            mFirebaseDatabaseReference.child(DATABASE)
                    .child(Teammates.DATABASE_USERS)
                    .child(myUserId).removeValue();
            startActivity(new Intent(this, SignInActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_send) {
            getSupportFragmentManager().popBackStackImmediate();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, TeammateFragment.newInstance(
                            getResources().getInteger(R.integer.grid_layout_size)),
                            getString(R.string.teammates))
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();

        } else if (id == R.id.nav_board) {
            getSupportFragmentManager().popBackStackImmediate();
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, BoardFragment.newInstance(), getString(R.string.team_board))
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();

        } else if (id == R.id.nav_user) {
            getSupportFragmentManager().popBackStackImmediate();
            getSupportActionBar().setElevation(0f);
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, MyFragment.newInstance(), getString(R.string.my_user))
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();

        } else if (id == R.id.nav_tutorial) {
            getSupportFragmentManager().popBackStackImmediate();
            startActivity(new Intent(getApplicationContext(), IntroActivity.class));

        } else if (id == R.id.nav_share) {
            startActivity(Intent.createChooser(ShareCompat.IntentBuilder.from(MainActivity.this)
                    .setType("text/plain")
                    .setText(getString(R.string.share_app_url))
                    .getIntent(), getString(R.string.share)));

        } else if (id == R.id.nav_settings) {
            startActivity(new Intent(getApplicationContext(), SettingsActivity.class));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onTeammateSelected(Teammates mate) {
        Intent intent = new Intent(this, MessageActivity.class);
        intent.putExtra(MessageFragment.ARG_ITEM_ID, mate.getId());
        intent.putExtra(MessageFragment.ARG_ITEM_NAME, mate.getName());
        startActivity(intent);
    }

    private void setupMyDeviceUser() {
        mFirebaseDatabaseReference.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                boolean alreadyRegistered = false;
                for (DataSnapshot userSnapshot : dataSnapshot.child(DATABASE)
                        .child(Teammates.DATABASE_USERS).getChildren()) {
                    if (userSnapshot.getKey().equals(myUserId)) {
                        alreadyRegistered = true;
                    }
                }
                if (!alreadyRegistered) {
                    Teammates user = new Teammates(myUserId, mUsername, mUserEmail, mPhotoUrl);
                    mFirebaseDatabaseReference.child(DATABASE)
                            .child(Teammates.DATABASE_USERS)
                            .child(myUserId).setValue(user);

                    Toast.makeText(getApplicationContext(),
                            String.format("%s %s", user.getName(), getString(R.string.user_registered)),
                            Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("", "loadUser:onCancelled", databaseError.toException());
                Toast.makeText(getApplicationContext(),
                        getString(R.string.user_not_registered), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void setFirebaseMessagesListener() {
        Query query = mFirebaseDatabaseReference.child(MainActivity.DATABASE)
                .child(MessageThanks.DATABASE_MESSAGES)
                .orderByChild(MessageThanks.TO_RECEIVER)
                .equalTo(myUserId);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    MessageThanks msg = userSnapshot.getValue(MessageThanks.class);
                    msg.setId(userSnapshot.getKey());
                    if (msg.getTimeRead() <= 0) {
                        Utilities.sendNotification(getApplicationContext(),
                                getString(R.string.thanks_received),
                                msg.getMessage(),
                                0,
                                msg.getFromSender());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("", "loadMessage:onCancelled", databaseError.toException());
            }
        });
    }

    private void getTeammateByIdAndSelect(String sender) {
        Query query = mFirebaseDatabaseReference.child(MainActivity.DATABASE)
                .child(Teammates.DATABASE_USERS)
                .orderByChild(Teammates.ID)
                .equalTo(sender);
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    onTeammateSelected(userSnapshot.getValue(Teammates.class));
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("", "loadTeammate:onCancelled", databaseError.toException());
            }
        });
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }
}
