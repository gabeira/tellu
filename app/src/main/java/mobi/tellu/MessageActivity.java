package mobi.tellu;

import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;

import mobi.tellu.fragment.MessageFragment;

public class MessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, MessageFragment.newInstance(
                            getIntent().getStringExtra(MessageFragment.ARG_ITEM_ID),
                            getIntent().getStringExtra(MessageFragment.ARG_ITEM_NAME)))
                    .setTransitionStyle(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
                    .commit();
        }
    }

}
