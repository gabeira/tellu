package mobi.tellu;

import android.app.Application;

/**
 * Created by gabriel on 4/5/17.
 */

public class TellUApp extends Application {

    public boolean justOpen;

    @Override
    public void onCreate() {
        super.onCreate();
        justOpen = true;
    }
}
