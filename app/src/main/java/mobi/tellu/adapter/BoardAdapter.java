package mobi.tellu.adapter;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;

import org.apache.commons.lang3.StringUtils;

import java.util.List;
import java.util.Locale;

import mobi.tellu.R;
import mobi.tellu.model.Teammates;

/**
 * {@link RecyclerView.Adapter} that can display a {@link Teammates}
 */
public class BoardAdapter extends RecyclerView.Adapter<BoardAdapter.ViewHolder> {

    private List<Teammates> mValues;

    public BoardAdapter(List<Teammates> items) {
        mValues = items;
    }

    public void setValues(List<Teammates> items) {
        mValues = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_teammate_board, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.mItem = mValues.get(position);
        holder.mCountView.setText(String.format(Locale.getDefault(), "%d",
                mValues.get(position).getThanksCount()));
        holder.mNameView.setText(mValues.get(position).getName());

        if (StringUtils.isNotEmpty(holder.mItem.getPhotoUrl())) {
            try {
                holder.mPhotoView.setContentDescription(holder.mItem.getName());

                Glide.with(holder.mPhotoView.getContext())
                        .load(mValues.get(position).getPhotoUrl())
                        .asBitmap().centerCrop()
                        .into(new BitmapImageViewTarget(holder.mPhotoView) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(
                                                holder.mPhotoView.getContext().getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                holder.mPhotoView.setImageDrawable(circularBitmapDrawable);
                            }
                        });
            } catch (Exception e) {
                holder.mPhotoView.setImageResource(R.drawable.ic_menu_user);
            }
        } else {
            holder.mPhotoView.setImageResource(R.drawable.ic_menu_user);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        final View mView;
        final ImageView mPhotoView;
        final TextView mNameView;
        final TextView mCountView;
        Teammates mItem;

        ViewHolder(View view) {
            super(view);
            mView = view;
            mPhotoView = (ImageView) view.findViewById(R.id.photo);
            mNameView = (TextView) view.findViewById(R.id.name);
            mCountView = (TextView) view.findViewById(R.id.thanksCount);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + mNameView.getText() + "'";
        }
    }
}
