package mobi.tellu.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import mobi.tellu.R;
import mobi.tellu.fragment.MyFragment.OnFragmentInteractionListener;
import mobi.tellu.model.MessageThanks;
import mobi.tellu.view.TextViewWithImages;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MessageThanks} and makes a call to the
 * specified {@link OnFragmentInteractionListener}.
 */
public class MessageAdapter extends RecyclerView.Adapter<MessageAdapter.ViewHolder> {

    private List<MessageThanks> mValues;
    private final OnFragmentInteractionListener mListener;

    public MessageAdapter(List<MessageThanks> items, OnFragmentInteractionListener listener) {
        mValues = items;
        mListener = listener;
    }

    public void setValues(List<MessageThanks> items) {
        mValues = items;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.message = mValues.get(position);
        if (holder.message.getMessage() != null) {
            holder.messageTextView.setText(holder.message.getMessage());
            holder.messageTextView.setVisibility(TextView.VISIBLE);
            if (holder.message.getTimeRead() <= 0) {
                if (mListener != null) {
                    mListener.onReadMessageInteraction(holder.message);
                }
            }
        } else {
            holder.messageTextView.setVisibility(TextView.GONE);
        }
        if (holder.message.getTimeSent() > 0) {
            DateFormat simpleDateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.MEDIUM, Locale.getDefault());
            holder.timeTextView.setText(simpleDateFormat.format(holder.message.getTimeSent()));
        } else {
            holder.timeTextView.setVisibility(TextView.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return mValues.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextViewWithImages messageTextView;
        ImageView starImageView;
        TextView timeTextView;
        MessageThanks message;

        ViewHolder(View v) {
            super(v);
            messageTextView = (TextViewWithImages) itemView.findViewById(R.id.messageTextView);
            starImageView = (ImageView) itemView.findViewById(R.id.starImageView);
            timeTextView = (TextView) itemView.findViewById(R.id.timeTextView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + messageTextView.getText() + "'";
        }
    }
}
