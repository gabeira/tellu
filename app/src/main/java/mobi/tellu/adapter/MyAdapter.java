package mobi.tellu.adapter;

import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.apache.commons.lang3.StringUtils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

import mobi.tellu.MainActivity;
import mobi.tellu.R;
import mobi.tellu.fragment.MyFragment.OnFragmentInteractionListener;
import mobi.tellu.model.MessageThanks;
import mobi.tellu.model.Teammates;
import mobi.tellu.view.TextViewWithImages;

/**
 * {@link RecyclerView.Adapter} that can display a {@link MessageThanks} and makes a call to the
 * specified {@link OnFragmentInteractionListener}.
 */
public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<MessageThanks> messageThanksList;
    private final OnFragmentInteractionListener mListener;
    private DatabaseReference mFirebaseDatabaseReference;

    public MyAdapter(List<MessageThanks> items, OnFragmentInteractionListener listener) {
        messageThanksList = items;
        mListener = listener;
    }

    public void setValues(List<MessageThanks> messageList) {
        messageThanksList = messageList;
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_message, parent, false);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.message = messageThanksList.get(position);
        if (holder.message.getMessage() != null) {
            holder.messageTextView.setText(holder.message.getMessage());
            holder.messageTextView.setVisibility(TextView.VISIBLE);
            if (holder.message.getTimeRead() <= 0) {
                if (mListener != null) {
                    mListener.onReadMessageInteraction(holder.message);
                }
            }
        } else {
            holder.messageTextView.setVisibility(TextView.GONE);
        }
        if (holder.message.getTimeSent() > 0) {
            DateFormat simpleDateFormat = SimpleDateFormat.getDateTimeInstance(DateFormat.DEFAULT, DateFormat.MEDIUM, Locale.getDefault());
            holder.timeTextView.setText(simpleDateFormat.format(holder.message.getTimeSent()));
        } else {
            holder.timeTextView.setVisibility(TextView.GONE);
        }

        Query querySenderPhoto = mFirebaseDatabaseReference.child(MainActivity.DATABASE)
                .child(Teammates.DATABASE_USERS)
                .orderByChild(Teammates.ID)
                .equalTo(holder.message.getFromSender());
        querySenderPhoto.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    Teammates teammate = userSnapshot.getValue(Teammates.class);
                    setSenderPhoto(holder.starImageView, teammate.getPhotoUrl());
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w("", "loadTeammate:onCancelled", databaseError.toException());
            }
        });
    }

    private void setSenderPhoto(final ImageView photoView, final String photoUrl) {
        photoView.setVisibility(View.VISIBLE);
        if (StringUtils.isNotEmpty(photoUrl)) {
            try {
                Glide.with(photoView.getContext())
                        .load(photoUrl)
                        .asBitmap().centerCrop()
                        .into(new BitmapImageViewTarget(photoView) {
                            @Override
                            protected void setResource(Bitmap resource) {
                                RoundedBitmapDrawable circularBitmapDrawable =
                                        RoundedBitmapDrawableFactory.create(
                                                photoView.getContext().getResources(), resource);
                                circularBitmapDrawable.setCircular(true);
                                photoView.setImageDrawable(circularBitmapDrawable);
                            }
                        });
            } catch (Exception e) {
                photoView.setImageResource(R.drawable.ic_menu_user);
            }
        } else {
            photoView.setImageResource(R.drawable.ic_menu_user);
        }
    }

    @Override
    public int getItemCount() {
        return messageThanksList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextViewWithImages messageTextView;
        ImageView starImageView;
        TextView timeTextView;
        MessageThanks message;

        ViewHolder(View v) {
            super(v);
            messageTextView = (TextViewWithImages) itemView.findViewById(R.id.messageTextView);
            starImageView = (ImageView) itemView.findViewById(R.id.starImageView);
            timeTextView = (TextView) itemView.findViewById(R.id.timeTextView);
        }

        @Override
        public String toString() {
            return super.toString() + " '" + messageTextView.getText() + "'";
        }
    }
}
