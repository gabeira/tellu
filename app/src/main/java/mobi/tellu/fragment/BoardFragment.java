package mobi.tellu.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import mobi.tellu.MainActivity;
import mobi.tellu.R;
import mobi.tellu.adapter.BoardAdapter;
import mobi.tellu.model.Teammates;

/**
 * A fragment representing a list of Items.
 */
public class BoardFragment extends Fragment {

    private static final String TAG = BoardFragment.class.getSimpleName();
    private ProgressBar mProgressBar;
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseAnalytics mFirebaseAnalytics;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public BoardFragment() {
    }

    @SuppressWarnings("unused")
    public static BoardFragment newInstance() {
        return new BoardFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_board, container, false);

        mFirebaseAnalytics.setCurrentScreen(getActivity(), getString(R.string.team_board), null);
        getActivity().setTitle(getString(R.string.team_board));
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.boardList);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
        recyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getContext(),
                linearLayoutManager.getOrientation());
        recyclerView.addItemDecoration(dividerItemDecoration);

        BoardAdapter boardAdapter = new BoardAdapter(new ArrayList<Teammates>());
        recyclerView.setAdapter(boardAdapter);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        setFirebaseTeammatesListener(boardAdapter);
        return view;
    }

    private void setFirebaseTeammatesListener(final BoardAdapter boardAdapter) {
        mProgressBar.setVisibility(ProgressBar.VISIBLE);

        Query queryReceiver = mFirebaseDatabaseReference.child(MainActivity.DATABASE)
                .child(Teammates.DATABASE_USERS)
                .orderByChild(Teammates.THANKS_COUNT);
        queryReceiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Teammates> teammatesList = new ArrayList<>();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    teammatesList.add(userSnapshot.getValue(Teammates.class));
                }
                Collections.reverse(teammatesList);
                boardAdapter.setValues(teammatesList);
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadTeammates:onCancelled", databaseError.toException());
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            }
        });
    }
}
