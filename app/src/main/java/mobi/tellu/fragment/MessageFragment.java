package mobi.tellu.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import mobi.tellu.MainActivity;
import mobi.tellu.R;
import mobi.tellu.adapter.MessageAdapter;
import mobi.tellu.model.Link;
import mobi.tellu.model.MessageThanks;
import mobi.tellu.model.Teammates;
import mobi.tellu.util.MessageParser;
import mobi.tellu.util.Utilities;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link MessageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MessageFragment extends Fragment implements
        GoogleApiClient.OnConnectionFailedListener {

    private static final String TAG = MessageFragment.class.getSimpleName();
    private Button mSendButton;
    private RecyclerView mMessageRecyclerView;
    private LinearLayoutManager mLinearLayoutManager;
    private ProgressBar mProgressBar;
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseAnalytics mFirebaseAnalytics;
    private EditText mMessageEditText;

    public static final String ARG_ITEM_ID = "param_id";
    public static final String ARG_ITEM_NAME = "param_name";

    private String mSelectedTeammateId;
    private String mSelectedTeammateUserName;
    private OkHttpClient httpClient = new OkHttpClient();

    public MessageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param selectedTeammateId   Parameter 1.
     * @param selectedTeammateName Parameter 2.
     * @return A new instance of fragment MessageFragment.
     */
    public static MessageFragment newInstance(String selectedTeammateId, String selectedTeammateName) {
        MessageFragment fragment = new MessageFragment();
        Bundle args = new Bundle();
        args.putString(ARG_ITEM_ID, selectedTeammateId);
        args.putString(ARG_ITEM_NAME, selectedTeammateName);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        if (getArguments() != null) {
            mSelectedTeammateId = getArguments().getString(ARG_ITEM_ID);
            mSelectedTeammateUserName = getArguments().getString(ARG_ITEM_NAME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        mFirebaseAnalytics.setCurrentScreen(getActivity(), getString(R.string.send_thanks), null);
        getActivity().setTitle(mSelectedTeammateUserName);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mLinearLayoutManager.setStackFromEnd(true);

        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mMessageRecyclerView = (RecyclerView) view.findViewById(R.id.messageRecyclerView);
        mMessageRecyclerView.setLayoutManager(mLinearLayoutManager);

        final MessageAdapter messageAdapter = new MessageAdapter(new ArrayList<MessageThanks>(), null);
        mMessageRecyclerView.setAdapter(messageAdapter);
        messageAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                super.onItemRangeInserted(positionStart, itemCount);
                int messageCount = messageAdapter.getItemCount();
                int lastVisiblePosition = mLinearLayoutManager.findLastCompletelyVisibleItemPosition();
                // If the recycler view is initially being loaded or the user is at the bottom of the list, scroll
                // to the bottom of the list to show the newly added message.
                if (lastVisiblePosition == -1 ||
                        (positionStart >= (messageCount - 1) && lastVisiblePosition == (positionStart - 1))) {
                    mMessageRecyclerView.scrollToPosition(positionStart);
                }
            }
        });
        setFirebaseMessagesListener(messageAdapter);

        mMessageEditText = (EditText) view.findViewById(R.id.messageEditText);
        mMessageEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().length() > 0) {
                    mSendButton.setEnabled(true);
                } else {
                    mSendButton.setEnabled(false);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        mSendButton = (Button) view.findViewById(R.id.sendButton);
        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!Utilities.isConnected(getContext())) {
                    Utilities.showUserLoginErrorAlert(getContext(), getString(R.string.no_internet_description), true);
                    return;
                }
                Runnable runnable = new Runnable() {
                    @Override
                    public void run() {
                        MessageThanks messageThanks = buildMessage(
                                FirebaseAuth.getInstance().getCurrentUser().getUid(),
                                mSelectedTeammateId,
                                mMessageEditText.getText().toString());
                        mFirebaseDatabaseReference.child(MainActivity.DATABASE).child(MessageThanks.DATABASE_MESSAGES).push().setValue(messageThanks);

                        //Increase count for message receiver
                        Query queryReceiver = mFirebaseDatabaseReference.child(MainActivity.DATABASE)
                                .child(Teammates.DATABASE_USERS)
                                .orderByKey()
                                .equalTo(mSelectedTeammateId);
                        queryReceiver.addListenerForSingleValueEvent(new ValueEventListener() {
                            @Override
                            public void onDataChange(DataSnapshot dataSnapshot) {
                                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                                    Teammates teammates = userSnapshot.getValue(Teammates.class);
                                    teammates.setId(userSnapshot.getKey());
                                    int count = teammates.getThanksCount();
                                    count++;
                                    mFirebaseDatabaseReference.child(MainActivity.DATABASE)
                                            .child(Teammates.DATABASE_USERS)
                                            .child(mSelectedTeammateId)
                                            .child(Teammates.THANKS_COUNT).setValue(count);
                                    Toast.makeText(getContext(),
                                            getString(R.string.send_successfully, mSelectedTeammateUserName),
                                            Toast.LENGTH_LONG).show();
                                }
                            }

                            @Override
                            public void onCancelled(DatabaseError databaseError) {
                                Log.w("", "loadTeammates:onCancelled", databaseError.toException());
                                Toast.makeText(getContext(),
                                        databaseError.getMessage().toString(), Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                };
                new Thread(runnable).start();
                mMessageEditText.setText("");
                mFirebaseAnalytics.logEvent(getString(R.string.send_successfully), null);
            }


        });
        return view;
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getContext(), connectionResult.getErrorMessage(),
                Toast.LENGTH_LONG).show();
        mProgressBar.setVisibility(ProgressBar.INVISIBLE);
    }

    private void setFirebaseMessagesListener(final MessageAdapter messageAdapter) {
        mProgressBar.setVisibility(ProgressBar.VISIBLE);
        Query query = mFirebaseDatabaseReference.child(MainActivity.DATABASE)
                .child(MessageThanks.DATABASE_MESSAGES)
                .orderByChild(MessageThanks.FROM_SENDER)
                .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<MessageThanks> messageList = new ArrayList<>();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    MessageThanks messageThanks = userSnapshot.getValue(MessageThanks.class);
                    messageThanks.setId(userSnapshot.getKey());
                    if (messageThanks.getToReceiver().equals(mSelectedTeammateId)) {
                        messageList.add(messageThanks);
                    }
                }
                messageAdapter.setValues(messageList);
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadMessages:onCancelled", databaseError.toException());
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            }
        });
    }

    public MessageThanks buildMessage(String fromSender, String toReceiver, String message) {
        MessageThanks messageThanks = new MessageThanks();
        messageThanks.setFromSender(fromSender);
        messageThanks.setToReceiver(toReceiver);
        messageThanks.setMessage(message);
        messageThanks.setTimeSent(Calendar.getInstance().getTimeInMillis());
        messageThanks.setTimeRead(0);

        messageThanks.setEmoticons(MessageParser.getEmoticons(getContext(), message));
        messageThanks.setMentions(MessageParser.getMentions(message));

        List<Link> links = new ArrayList<>();
        for (String url : MessageParser.getLinks(message)) {
            links.add(new Link(url, getLinkTitle(url)));
        }
        messageThanks.setLinks(links);
        return messageThanks;
    }


    private String getLinkTitle(final String linkUrl) {
        try {
            return MessageParser.getLinkTitle(doGetRequest(linkUrl));
        } catch (IOException e) {
            return "";
        }
    }

    String doGetRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = httpClient.newCall(request).execute();
        return response.body().string();
    }
}
