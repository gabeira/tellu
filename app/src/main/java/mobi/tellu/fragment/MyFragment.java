package mobi.tellu.fragment;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;

import mobi.tellu.MainActivity;
import mobi.tellu.R;
import mobi.tellu.adapter.MyAdapter;
import mobi.tellu.model.MessageThanks;

/**
 * A fragment representing the User information and Thanks received.
 * Activities that contain this fragment must implement the
 * {@link OnFragmentInteractionListener} interface
 * to handle interaction events.
 */
public class MyFragment extends Fragment {

    private final String TAG = MyFragment.class.getSimpleName();
    private RecyclerView myThanksRecyclerView;
    private ProgressBar mProgressBar;
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseAnalytics mFirebaseAnalytics;
    private ImageView myPhotoView;

    private OnFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public MyFragment() {
    }

    @SuppressWarnings("unused")
    public static MyFragment newInstance() {
        return new MyFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my, container, false);
        mFirebaseAnalytics.setCurrentScreen(getActivity(), getString(R.string.my_user), null);
        getActivity().setTitle(getString(R.string.my_thanks_received));
        final Context context = view.getContext();
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();

        myThanksRecyclerView = (RecyclerView) view.findViewById(R.id.my_thanks_list);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
        myThanksRecyclerView.setLayoutManager(linearLayoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(context,
                linearLayoutManager.getOrientation());
        myThanksRecyclerView.addItemDecoration(dividerItemDecoration);

        mListener = new OnFragmentInteractionListener() {
            @Override
            public void onReadMessageInteraction(MessageThanks msg) {
                //Update message read time for now
                mFirebaseDatabaseReference.child(MainActivity.DATABASE)
                        .child(MessageThanks.DATABASE_MESSAGES)
                        .child(msg.getId())
                        .child(MessageThanks.TIME_READ).setValue(Calendar.getInstance().getTimeInMillis());
            }
        };
        final MyAdapter messageAdapter = new MyAdapter(new ArrayList<MessageThanks>(), mListener);

        myThanksRecyclerView.setAdapter(messageAdapter);
        setFirebaseMessagesListener(messageAdapter);

        FirebaseUser mFirebaseUser = FirebaseAuth.getInstance().getCurrentUser();
        if (mFirebaseUser != null) {
            myPhotoView = (ImageView) view.findViewById(R.id.my_photo);
            final String photoUrl = mFirebaseUser.isAnonymous() ?
                    getString(R.string.anonymous_photo_url) :
                    mFirebaseUser.getPhotoUrl().toString();
            if (StringUtils.isNotEmpty(photoUrl)) {
                try {
                    myPhotoView.setContentDescription(mFirebaseUser.getDisplayName());

                    Glide.with(context)
                            .load(photoUrl)
                            .asBitmap().centerCrop()
                            .into(new BitmapImageViewTarget(myPhotoView) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(
                                                    context.getResources(), resource);
                                    circularBitmapDrawable.setCircular(true);
                                    myPhotoView.setImageDrawable(circularBitmapDrawable);
                                }
                            });
                } catch (Exception e) {
                    myPhotoView.setImageResource(R.drawable.ic_menu_user);
                }
            } else {
                myPhotoView.setImageResource(R.drawable.ic_menu_user);
            }

            TextView myName = (TextView) view.findViewById(R.id.my_name);
            myName.setText(mFirebaseUser.isAnonymous() ? MainActivity.ANONYMOUS : mFirebaseUser.getDisplayName());
            TextView myEmail = (TextView) view.findViewById(R.id.my_email);
            myEmail.setText(mFirebaseUser.getEmail());
        }
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onReadMessageInteraction(MessageThanks msg);
    }

    private void setFirebaseMessagesListener(final MyAdapter messageAdapter) {
        mProgressBar.setVisibility(ProgressBar.VISIBLE);
        Query query = mFirebaseDatabaseReference.child(MainActivity.DATABASE)
                .child(MessageThanks.DATABASE_MESSAGES)
                .orderByChild(MessageThanks.TO_RECEIVER)
                .equalTo(FirebaseAuth.getInstance().getCurrentUser().getUid());
        query.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<MessageThanks> messageList = new ArrayList<>();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    MessageThanks messageThanks = userSnapshot.getValue(MessageThanks.class);
                    messageThanks.setId(userSnapshot.getKey());
                    messageList.add(messageThanks);
                }
                Collections.reverse(messageList);
                messageAdapter.setValues(messageList);
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadMessages:onCancelled", databaseError.toException());
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            }
        });
    }
}
