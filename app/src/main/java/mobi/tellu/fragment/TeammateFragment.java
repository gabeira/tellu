package mobi.tellu.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import mobi.tellu.MainActivity;
import mobi.tellu.R;
import mobi.tellu.adapter.TeammatesAdapter;
import mobi.tellu.model.Teammates;

/**
 * A fragment representing a list of Items.
 * <p/>
 * Activities containing this fragment MUST implement the {@link OnListFragmentInteractionListener}
 * interface.
 */
public class TeammateFragment extends Fragment {

    private static final String TAG = TeammateFragment.class.getSimpleName();
    private ProgressBar mProgressBar;
    private DatabaseReference mFirebaseDatabaseReference;
    private FirebaseAnalytics mFirebaseAnalytics;
    private static final String ARG_COLUMN_COUNT = "column-count";
    private int mColumnCount = 1;
    private OnListFragmentInteractionListener mListener;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public TeammateFragment() {
    }

    public static TeammateFragment newInstance(int columnCount) {
        TeammateFragment fragment = new TeammateFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mFirebaseAnalytics = FirebaseAnalytics.getInstance(getContext());
        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_teammate, container, false);
        mFirebaseAnalytics.setCurrentScreen(getActivity(), getString(R.string.teammates), null);
        getActivity().setTitle(getString(R.string.teammates));
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        Context context = view.getContext();
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.teammatesRecyclerView);
        if (mColumnCount <= 1) {
            recyclerView.setLayoutManager(new LinearLayoutManager(context));
        } else {
            recyclerView.setLayoutManager(new GridLayoutManager(context, mColumnCount));
        }
        final TeammatesAdapter teammatesAdapter = new TeammatesAdapter(new ArrayList<Teammates>(), mListener);
        recyclerView.setAdapter(teammatesAdapter);
        mFirebaseDatabaseReference = FirebaseDatabase.getInstance().getReference();
        setFirebaseTeammatesListener(teammatesAdapter);
        return view;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnListFragmentInteractionListener) {
            mListener = (OnListFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnListFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnListFragmentInteractionListener {
        void onTeammateSelected(Teammates item);
    }

    private void setFirebaseTeammatesListener(final TeammatesAdapter teammatesAdapter) {
        mProgressBar.setVisibility(ProgressBar.VISIBLE);

        Query queryReceiver = mFirebaseDatabaseReference.child(MainActivity.DATABASE)
                .child(Teammates.DATABASE_USERS)
                .orderByChild(Teammates.USER_NAME);
        queryReceiver.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                List<Teammates> teammatesList = new ArrayList<>();
                for (DataSnapshot userSnapshot : dataSnapshot.getChildren()) {
                    try {
                        if (!userSnapshot.getKey().equals(FirebaseAuth.getInstance().getCurrentUser().getUid())) {
                            Teammates teammates = userSnapshot.getValue(Teammates.class);
                            teammates.setId(userSnapshot.getKey());
                            teammatesList.add(teammates);
                        }
                    } catch (Exception e) {
                    }
                }
                teammatesAdapter.setValues(teammatesList);
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.w(TAG, "loadTeammates:onCancelled", databaseError.toException());
                mProgressBar.setVisibility(ProgressBar.INVISIBLE);
            }
        });
    }
}
