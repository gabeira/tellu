package mobi.tellu.model;

import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by gabriel on 3/26/17.
 */
@IgnoreExtraProperties
public class Link {

    private String url;
    private String title;

    public Link() {
    }

    public Link(String url, String title) {
        this.url = url;
        this.title = title;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
