package mobi.tellu.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.List;

/**
 * Created by gabriel on 3/26/17.
 */
@IgnoreExtraProperties
public class MessageThanks {

    @Exclude
    public static final String DATABASE_MESSAGES = "messages";
    @Exclude
    public static final String FROM_SENDER = "fromSender";
    @Exclude
    public static final String TO_RECEIVER = "toReceiver";
    @Exclude
    public static final String TIME_READ = "timeRead";
    @Exclude
    private String id;
    private String fromSender;
    private String toReceiver;
    private String message;
    private long timeSent;
    private long timeRead;
    private List<String> mentions;
    private List<String> emoticons;
    private List<Link> links;

    public MessageThanks() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFromSender() {
        return fromSender;
    }

    public void setFromSender(String fromSender) {
        this.fromSender = fromSender;
    }

    public String getToReceiver() {
        return toReceiver;
    }

    public void setToReceiver(String toReceiver) {
        this.toReceiver = toReceiver;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getTimeSent() {
        return timeSent;
    }

    public void setTimeSent(long timeSent) {
        this.timeSent = timeSent;
    }

    public long getTimeRead() {
        return timeRead;
    }

    public void setTimeRead(long timeRead) {
        this.timeRead = timeRead;
    }

    public List<String> getMentions() {
        return mentions;
    }

    public void setMentions(List<String> mentions) {
        this.mentions = mentions;
    }

    public List<String> getEmoticons() {
        return emoticons;
    }

    public void setEmoticons(List<String> emoticons) {
        this.emoticons = emoticons;
    }

    public List<Link> getLinks() {
        return links;
    }

    public void setLinks(List<Link> links) {
        this.links = links;
    }

}