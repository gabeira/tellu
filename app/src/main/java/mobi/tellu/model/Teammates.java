package mobi.tellu.model;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by gabriel on 3/26/17.
 */
@IgnoreExtraProperties
public class Teammates {

    @Exclude
    public static final String DATABASE_USERS = "users";
    @Exclude
    public static final String USER_NAME = "username";
    @Exclude
    public static final String THANKS_COUNT = "thanksCount";
    @Exclude
    public static final String ID = "id";
    @Exclude
    private String id;
    private String name;
    private String email;
    private int thanksCount;
    private String photoUrl;

    public Teammates() {
    }

    public Teammates(String id, String name, String email, String photoUrl) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.photoUrl = photoUrl;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getThanksCount() {
        return thanksCount;
    }

    public void setThanksCount(int thanksCount) {
        this.thanksCount = thanksCount;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }
}