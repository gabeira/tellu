package mobi.tellu.service;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.apache.commons.lang3.StringUtils;

import mobi.tellu.R;
import mobi.tellu.util.Utilities;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // Check if message contains a data payload.
        if (remoteMessage.getNotification() != null && remoteMessage.getNotification().getBody() != null) {
            String title = remoteMessage.getNotification().getTitle();
            if (StringUtils.isEmpty(title)) {
                title = getString(R.string.app_name);
            }
            Utilities.sendNotification(getApplicationContext(), title, remoteMessage.getNotification().getBody(), 0, null);
        }
    }

}