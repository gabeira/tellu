package mobi.tellu.util;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by gabriel on 3/25/17.
 */

public class MessageParser {

    private static final Pattern mentionsPattern = Pattern.compile("@([a-zA-Z0-9_]+)");
    private static final Pattern emoticonsPattern = Pattern.compile("\\((.*?)\\)");
    private static final Pattern linksPattern = Pattern.compile("\\W?([^ ]+://[^ ]+)\\W?");
    private static final Pattern linkTitlePatten = Pattern.compile("<title>(.*?)</title>");

    public static List<String> getMentions(String message) {
        Matcher matcher = mentionsPattern.matcher(message);
        List<String> mentions = new ArrayList<>();
        while (matcher.find()) {
            mentions.add(matcher.group(1));
        }
        return mentions;
    }

    public static List<String> getEmoticons(Context context, String message) {
        Matcher matcher = emoticonsPattern.matcher(message);
        List<String> emoticons = new ArrayList<>();
        while (matcher.find()) {
            String found = matcher.group(1);
            if (found.equalsIgnoreCase("continue")) found = "continua";//Workaround for restricted word continue
            int id = context.getResources().getIdentifier(found.toLowerCase(), "drawable", context.getPackageName());
            if (id != 0) {
                emoticons.add(found);
            }
        }
        return emoticons;
    }

    public static List<String> getLinks(String message) {
        Matcher matcher = linksPattern.matcher(message);
        List<String> links = new ArrayList<>();
        while (matcher.find()) {
            links.add(matcher.group(1));
        }
        return links;
    }

    public static String getLinkTitle(String message) {
        Matcher matcher = linkTitlePatten.matcher(message);
        return matcher.find() ? matcher.group(1) : "";
    }
}
