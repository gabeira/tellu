package mobi.tellu.util;

import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v7.app.AlertDialog;

import org.apache.commons.lang3.StringUtils;

import mobi.tellu.MainActivity;
import mobi.tellu.R;
import mobi.tellu.model.MessageThanks;

/**
 * Created by gabriel on 3/25/17.
 */

public class Utilities {

    public static void sendNotification(Context context,
                                        @NonNull String title,
                                        @NonNull String messageBody,
                                        @NonNull Integer notificationId,
                                        String fromSender) {

        Intent intent = new Intent(context, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra(MainActivity.IS_FROM_NOTIFICATION, true);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(context)
                .setSmallIcon(R.drawable.ic_stat_ic_notification)
                .setContentTitle(title)
                .setStyle(new NotificationCompat.BigTextStyle()
                        .bigText(messageBody))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setColor(context.getResources().getColor(R.color.primary_dark))
                .setGroup(context.getString(R.string.app_name))
                .setGroupSummary(true)
                .setContentIntent(pendingIntent);


        if (StringUtils.isNotEmpty(fromSender)) {
            // Create an intent for the reply action
            Intent actionIntent = new Intent(context, MainActivity.class);
            actionIntent.putExtra(MainActivity.IS_FROM_NOTIFICATION, true);
            actionIntent.putExtra(MessageThanks.FROM_SENDER, fromSender);
            PendingIntent actionPendingIntent =
                    PendingIntent.getActivity(context, 0, actionIntent, PendingIntent.FLAG_ONE_SHOT);

            // Create Reply Action for Wearable
            NotificationCompat.Action action = new NotificationCompat.Action.Builder(
                    R.drawable.ic_menu_send,
                    context.getString(R.string.reply),
                    actionPendingIntent).build();

            notificationBuilder.extend(new NotificationCompat.WearableExtender().addAction(action));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher_round));
        } else {
            notificationBuilder.setLargeIcon(BitmapFactory.decodeResource(context.getResources(), R.mipmap.ic_launcher));
        }

        NotificationManagerCompat.from(context).notify(notificationId, notificationBuilder.build());
    }

    public static boolean isConnected(final Context context) {
        ConnectivityManager cm =
                (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        return activeNetwork != null && activeNetwork.isConnectedOrConnecting();
    }

    public static void showUserLoginErrorAlert(final Context context, String errorDescription, boolean showWifiSettings) {
        AlertDialog.Builder alert = new AlertDialog.Builder(context)
                .setTitle(R.string.sign_in_error)
                .setMessage(errorDescription)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
        if (showWifiSettings) {
            alert.setTitle(R.string.no_internet_title);
            alert.setMessage(R.string.no_internet_description);
            alert.setNegativeButton(R.string.action_settings, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    context.startActivity(new Intent(Settings.ACTION_WIFI_SETTINGS));
                }
            });
        }
        alert.show();
    }
}
