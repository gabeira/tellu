package mobi.tellu;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import mobi.tellu.util.MessageParser;

import static org.junit.Assert.assertEquals;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(MockitoJUnitRunner.class)
public class ParseMessageUnitTest {

    @Test
    public void messageMentionsParsing() throws Exception {
        List<String> mentions = MessageParser.getMentions("this is@ a @test for @s_o_m_e @kind @.f @mentions.");
        assertEquals(4, mentions.size());
        assertEquals(mentions.get(0), "test");
        assertEquals(mentions.get(1), "s_o_m_e");
        assertEquals(mentions.get(2), "kind");
        assertEquals(mentions.get(3), "mentions");
    }

    @Test
    public void messageLinkParsing() {
        List<String> links = MessageParser.getLinks("This is a website https://www.google.com and also http this http://tellu.com:8888/test/android .");
        assertEquals(2, links.size());
        assertEquals("https://www.google.com", links.get(0));
        assertEquals("http://tellu.com:8888/test/android", links.get(1));
    }

    @Test
    public void messageLinkTitleParsing() throws Exception {
        assertEquals("This is the main page title",
                MessageParser.getLinkTitle("<html>test<title>This is the main page title</title>other<body></body></html>"));
    }

}